package com.ibeetl.dao.mybatis.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;

@Configuration
@MapperScan("com.ibeetl.dao.myabtis.mapper")
public class MyBatisPlusConfig {
		@Bean
	   public PaginationInterceptor paginationInterceptor() {
	      return new PaginationInterceptor();
	   }
}
